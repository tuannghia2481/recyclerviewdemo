package com.example.recyclerview_demo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.GridLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ProductAdapter.IOnClickItem {

    List<Product> listProduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();

        ProductAdapter adapter = new ProductAdapter(this,listProduct,this);

        GridLayoutManager layoutManager = new GridLayoutManager(this,2);

        RecyclerView rvProduct = (RecyclerView) findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData(){
        listProduct.add(new Product("Zara P1","Love Shift 1","1100.000",R.drawable.ic_user_a));
        listProduct.add(new Product("Zara P2","Love Shift 2","1200.000",R.drawable.ic_user_b));
        listProduct.add(new Product("Zara P3","Love Shift 3","1300.000",R.drawable.ic_user_c));
        listProduct.add(new Product("Zara P4","Love Shift 4","1400.000",R.drawable.ic_user_d));
        listProduct.add(new Product("Zara P5","Love Shift 5","1500.000",R.drawable.ic_user_a));
        listProduct.add(new Product("Zara P6","Love Shift 6","1600.000",R.drawable.ic_user_b));
        listProduct.add(new Product("Zara P7","Love Shift 7","1700.000",R.drawable.ic_user_c));
        listProduct.add(new Product("Zara P8","Love Shift 8","1800.000",R.drawable.ic_user_d));
    }

    @Override
    public void onClickItem(int positon) {
Product product = listProduct.get(positon);
        Toast.makeText(this,product.getTitle(),Toast.LENGTH_SHORT).show();
    }
}
